/*
 * EthRecv.c
 *
 *  Created on: Nov 25, 2021
 *
 *      Author: 68226
 */

#include "cw2.h"
#if ZADANIE == 2
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/if_ether.h>

// Definiowanie dlugosci pol naglowka IPv4
#define VERSION_LEN 1
#define IHL_LEN 1
#define TOS_LEN 1
#define TOTAL_LEN 2
#define IDENTIFICATION_LEN 2
#define FLAGS_LEN 1
#define FRAGMENTOFF_LEN 2
#define TTL_LEN 1
#define PROTOCOL_LEN 1
#define HEADERCHECK_LEN 2
#define SOURCE_ADDR_LEN 4
#define DESTINATION_LEN 4

// Definiowanie pozycji danych pol
#define VERSION_POS 14
#define IHL_POS 14
#define TOS_POS 15
#define TOTAL_POS 16
#define IDENTIFICATION_POS 18
#define FLAGS_POS 20

//#define FRAGMENTOFF_POS 2
#define TTL_POS 22
#define PROTOCOL_POS 23
#define HEADERCHECK_POS 24
#define SOURCE_ADDR_POS 26
#define DESTINATION_POS 30

// Definiowanie dlugosci pol ICMP
#define TYPE_LEN 1
#define CODE_LEN 1
#define CHECKSUM_LEN 2
#define IDENTIFIER_LEN 2
#define SEQUENCE_LEN 2
#define TIMESTAMP_LEN 8
#define DATA_LEN

// Definiowanie pozycji pol ICMP
#define TYPE_POS 34
#define CODE_POS 35
#define CHECKSUM_POS 36
#define IDENTIFIER_POS 38
#define SEQUENCE_POS 40
#define TIMESTAMP_POS 42
#define DATA_POS 50

int main(void) {
	unsigned char version;
	unsigned char ihl;
	unsigned char tos;
	unsigned char total_len[2];
	unsigned char identification[2];
	unsigned char flags;
	unsigned char ttl;
	unsigned char protocol;
	unsigned char headercheck[2];
	unsigned char source_addr[4];
	unsigned char destination_addr[4];

	unsigned char type;
	unsigned char code;
	unsigned char checksum[2];
	unsigned char identifier[2];
	unsigned char sequence[2];
	unsigned char timestamp[8];
	unsigned char icmp_data;

	printf("Uruchamiam odbieranie ramek Ethernet.\n"); /* prints */
//Utworzenie bufora dla odbieranych ramek Ethernet
	unsigned char *buffer = (void*) malloc(ETH_FRAME_LEN);
//Otwarcie gniazda pozwalającego na odbiór wszystkich ramek Ethernet
	int iEthSockHandl = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
//Kontrola czy gniazdo zostało otwarte poprawnie, w przypadku bledu wyświetlenie komunikatu.
	if (iEthSockHandl < 0)
		printf("Problem z otwarciem gniazda : %s!\n", strerror(errno));
//Zmienna do przechowywania rozmiaru odebranych danych
	int iDataLen = 0;
	int i;
	unsigned short typ_ramki;
//Pętla nieskończona do odbierania ramek Ethernet
	while (1) {
//Odebranie ramki z utworzonego wcześniej gniazda i zapisanie jej do bufora
		iDataLen = recvfrom(iEthSockHandl, buffer, ETH_FRAME_LEN, 0, NULL,
		NULL);
//Kontrola czy nie było bledu podczas odbierania ramki
		if (iDataLen == -1)
			printf("Nie moge odebrac ramki: %s! \n", strerror(errno));
		else //jeśli ramka odebrana poprawnie wyświetlenie jej zawartości
		{
			printf("\nOdebrano ramke Ethernet o rozmiarze: %d [B]\n", iDataLen);
			for (i = 0; i < iDataLen; i++) {
				printf("%.2x, ", buffer[i]);
			}

			printf("\n");
			memcpy(&typ_ramki, buffer + 12, 2);
			typ_ramki = ntohs(typ_ramki);
			printf("Typ ramki: %x \n", typ_ramki);
			if (typ_ramki == 0x0800) {
				printf("+++++To jest ramka IPv4+++++");
				printf("\n");

				memcpy(&version, buffer + VERSION_POS, VERSION_LEN);
				version = version & 0xF0;
				memcpy(&ihl, buffer + IHL_POS, IHL_LEN);
				ihl = ihl & 0xF;
				memcpy(&tos, buffer + TOS_POS, TOS_LEN);
				memcpy(&total_len, buffer + TOTAL_POS, TOTAL_LEN);
				memcpy(&identification, buffer + IDENTIFICATION_POS,
						IDENTIFICATION_LEN);
				memcpy(&flags, buffer + FLAGS_POS, FLAGS_LEN);
				memcpy(&ttl, buffer + TTL_POS, TTL_LEN);
				memcpy(&protocol, buffer + PROTOCOL_POS, PROTOCOL_LEN);
				memcpy(&headercheck, buffer + HEADERCHECK_POS, HEADERCHECK_LEN);
				memcpy(&source_addr, buffer + SOURCE_ADDR_POS, SOURCE_ADDR_LEN);
				memcpy(&destination_addr, buffer + DESTINATION_POS,
						DESTINATION_LEN);

				printf("Pole wersji\t\t\t[rozmiar %.d bajtow]: %.2x\n",
						VERSION_LEN, version);
				printf("Pole ihl\t\t\t[rozmiar %.d bajtow]: %.2x\n", IHL_LEN,
						ihl);
				printf("Pole TOS\t\t\t[rozmiar %.d bajtow]: %.2x \n", TOS_LEN,
						tos);
				printf("Pole total len\t\t\t[rozmiar %.d bajtow]: %.2x %.2x\n",
						TOTAL_LEN, total_len[0], total_len[1]);
				printf(
						"Pole identification\t\t[rozmiar %.d bajtow]: %.2x %.2x\n",
						IDENTIFICATION_LEN, identification[0],
						identification[1]);
				printf("Pole flags\t\t\t[rozmiar %.d bajtow]: %.2x\n",
						FLAGS_LEN, flags);
				printf("Pole ttl\t\t\t[rozmiar %.d bajtow]: %.2x\n", TTL_LEN,
						ttl);
				printf("Pole protocol\t\t\t[rozmiar %.d bajtow]: %.2x\n",
						PROTOCOL_LEN, protocol);
				printf(
						"Pole header checksum\t\t[rozmiar %.d bajtow]: %.2x, %.2x\n",
						HEADERCHECK_LEN, headercheck[0], headercheck[1]);
				printf(
						"Pole source address\t\t[rozmiar %.d bajtow]: %.2x %.2x %.2x %.2x\n",
						SOURCE_ADDR_LEN, source_addr[0], source_addr[1],
						source_addr[2], source_addr[3]);
				printf(
						"Pole destination address\t[rozmiar %.d bajtow]: %.2x %.2x %.2x %.2x\n",
						DESTINATION_LEN, destination_addr[0],
						destination_addr[1], destination_addr[2],
						destination_addr[3]);

				if (protocol == 1) {
					printf("\n\n +++++To jest wiadomosc ICMP+++++\n\n");
					memcpy(&type, buffer + TYPE_POS, TYPE_LEN);
					memcpy(&code, buffer + CODE_POS, CODE_LEN);
					memcpy(&checksum, buffer + CHECKSUM_POS, CHECKSUM_LEN);
					memcpy(&identifier, buffer + IDENTIFIER_POS,
							IDENTIFIER_LEN);
					memcpy(&sequence, buffer + SEQUENCE_POS, SEQUENCE_LEN);
					memcpy(&timestamp, buffer + TIMESTAMP_POS, TIMESTAMP_LEN);

					printf("Pole type\t\t\t[rozmiar %.d bajtow]: %.2x\n",
							TYPE_LEN, type);
					printf("Pole code\t\t\t[rozmiar %.d bajtow]: %.2x\n",
							CODE_LEN, code);
					printf(
							"Pole checksum\t\t\t[rozmiar %.d bajtow]: %.2x %.2x\n",
							CHECKSUM_LEN, checksum[0], checksum[1]);
					printf(
							"Pole identifier\t\t\t[rozmiar %.d bajtow]: %.2x %.2x\n",
							IDENTIFIER_LEN, identifier[0], identifier[1]);
					printf(
							"Pole sequence\t\t\t[rozmiar %.d bajtow]: %.2x %.2x\n",
							SEQUENCE_LEN, sequence[0], sequence[1]);
					printf(
							"Pole timestamp\t\t\t[rozmiar %.d bajtow]: %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x\n",
							TIMESTAMP_LEN, timestamp[0], timestamp[1],
							timestamp[2], timestamp[3], timestamp[4],
							timestamp[5], timestamp[6], timestamp[7]);
					printf("Dane wiadomosci ICMP: ");
					for (i = DATA_POS; i < iDataLen; i++)
						printf("%.2x, ", buffer[i]);
					printf("\n\n\n");
				}

			}
		}
	}
	return EXIT_SUCCESS;
}
#endif
